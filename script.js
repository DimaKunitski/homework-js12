var slides = document.querySelectorAll('#slides .slide');
var currentSlide = 0;
var slideInterval = setInterval(nextSlide,2000);
function nextSlide(){
    slides[currentSlide].className = 'slide';
    currentSlide = (currentSlide+1)%slides.length;
    slides[currentSlide].className = 'slide showing';
}

let playing = true;
const pauseButton = document.getElementById('pause');
const playButton = document.getElementById('play');
pauseButton.onclick = function pauseSlideShow(){
    playing = false;
    clearInterval(slideInterval);
    if(playing){pauseSlideShow();
    }
}
playButton.onclick = function playSlideShow (){
    playing = true;
    slideInterval = setInterval(nextSlide,2000);
    if(playing !== true) {playSlideShow()
    }
}


